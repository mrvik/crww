package crww

import (
    "bytes"
    "crypto/rand"
    "io"
    "io/ioutil"
    "net/http"
    "net/http/httptest"
    "testing"
)

//CRWW must implement ResponseWriter and WriteCloser
var _ http.ResponseWriter= &CRWW{}
var _ io.WriteCloser= &CRWW{}

var t_methods=append([]CompressorSpec{
    {
        Name: "",
    },
}, Methods...)

func TestWrapper(t *testing.T){
    var testData [8192]byte
    bs, err:=rand.Reader.Read(testData[:len(testData)/2])
    if err!=nil{
        panic(err)
    }
    t.Logf("Read %d bytes\n", bs)
    for _,method:=range(t_methods){
        t.Run("method="+method.Name, func(t *testing.T){
            expectedSize:=calcSize(testData[:], method.Fn)
            recorder:=httptest.NewRecorder()
            req:=http.Request{
                Header: http.Header{
                    "Accept-Encoding": []string{method.Name},
                },
            }
            //Obtain the wrapper
            code:=expectedSize%300+200
            wrapper:=NewCRWW(recorder, &req)
            wrapper.Header().Set("Content-Type", "application/octet-stream")
            wrapper.WriteHeader(code) //Random code
            _, err=wrapper.Write(testData[:len(testData)/2])
            if err!=nil{
                panic(err)
            }
            _, err=wrapper.Write(testData[len(testData)/2:])
            if err!=nil{
                panic(err)
            }
            wrapper.Close() //Should block until all things are ready
            //Test exit data
            t.Logf("Body has %d bytes\n", recorder.Body.Len())
            res:=recorder.Result()
            buf, err:=ioutil.ReadAll(res.Body)
            if err!=nil{
                panic(err)
            }
            res.Body.Close()
            finalLen:=len(buf)
            if finalLen!=expectedSize{
                t.Errorf("Expected response to have %d bytes but had %d instead\n", expectedSize, finalLen)
            }else{
                t.Logf("Got %d bytes as expected", finalLen)
            }
            if selectedEncoding:=res.Header.Get("Content-Encoding"); selectedEncoding!=method.Name{
                t.Errorf("Expected response to be encoded as %s but got %s instead\n", method.Name, selectedEncoding)
            }else{
                t.Logf("Got response encoded as %q as expected\n", method.Name)
            }
            if code!=res.StatusCode{
                t.Errorf("Expected to have code %d but got %d\n", code, res.StatusCode)
            }else{
                t.Logf("Got code %d as expected\n", res.StatusCode)
            }
        })
    }
}

func calcSize(of []byte, with CompressorFunc) int{
    if with==nil{
        return len(of)
    }
    var buf bytes.Buffer
    with(bytes.NewReader(of), &buf, func(){})
    return buf.Len()
}
