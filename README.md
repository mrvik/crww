# Compressing Response Writer Wrapper

This lib wraps an `http.ResponseWriter` which compresses requests if the client does support it.

Docs on [pkg.go.dev](https://pkg.go.dev/gitlab.com/mrvik/crww/) as usual.
