package gz

import (
    "compress/gzip"
    "io"
)

func Compress(in io.Reader, out io.Writer, done func()){
    defer done()
    encoder:=gzip.NewWriter(out)
    var buf [128]byte
    _, err:=io.CopyBuffer(encoder, in, buf[:])
    if err!=nil{
        panic(err)
    }
    encoder.Close()
}
