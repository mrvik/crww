module gitlab.com/mrvik/crww

go 1.15

require (
	github.com/DataDog/zstd v1.4.5
	github.com/itchio/go-brotli v0.0.0-20190702114328-3f28d645a45c
)
