// This package holds a living example of working with CRWW using the adapter package.
package main

import (
    "gitlab.com/mrvik/crww/adapter"
    "net/http"
)

var staticServer=http.FileServer(http.Dir("."))

func main(){
    http.ListenAndServe(":8088", adapter.HandlerWrapper{
        Handler: staticServer,
    })
}
