// package crww holds the main logic to wrap the ResponseWriter.
// For ease of use, look on the adapter package.
// The example subdir has a working static server wrapped with CRWW using the adapter package
package crww

import (
    "bufio"
    "context"
    "io"
    "net/http"
)

type CRWW struct{
    wrap http.ResponseWriter
    req *http.Request
    compressor *bufio.Writer
    ctx context.Context
    close func() error
}

//Wrap a ResponseWriter. Original one must **not** be used after passed to this function. Also the req is needed to find a suitable compression method.
//Keep in mind you must call Close in order to end sending response
func NewCRWW(wrap http.ResponseWriter, req *http.Request) *CRWW{
    r:=CRWW{
        wrap: wrap,
        req: req,
    }
    r.selectCompressor()
    return &r
}

func (crww *CRWW) selectCompressor(){
    name, compressor:=DetectCompression(crww.req.Header.Get("Accept-Encoding"))
    var writer io.Writer
    ctx, done:=context.WithCancel(crww.req.Context())
    crww.ctx=ctx
    switch name{
    case "":
        writer=crww.wrap //Use ResponseWriter as io.Writer directly
        defer done() //No goroutines are spawned
    default:
        crww.wrap.Header().Set("Content-Encoding", name)
        pr,pw:=io.Pipe()
        crww.close=pw.Close
        writer=pw
        go compressor(pr, crww.wrap, done)
    }
    crww.compressor=bufio.NewWriter(writer)
}

//Goes directly to the original ResponseWriter. You shall not touch the Content-Encoding header unless you like garbage.
func (crww *CRWW) Header() http.Header{
    return crww.wrap.Header()
}

//Implements io.Writer.
//Write operations are buffered so it won't block unless buffer size is exhausted.
func (crww *CRWW) Write(in []byte) (count int, err error){
    return crww.compressor.Write(in)
}

//Directly sent to original ResponseWriter
func (crww *CRWW) WriteHeader(code int){
    crww.wrap.WriteHeader(code)
}

//Close function must be called in order to flush written response and close everything
//Close will block until all buffer data is consumed and sent to the original ResponseWriter.
func (crww *CRWW) Close() error{
    err:=crww.compressor.Flush()
    if err!=nil{
        return err
    }
    if crww.close!=nil{
        crww.close()
    }
    //Wait for compressor to stop...
    <-crww.ctx.Done()
    return nil
}
