package zstd

import (
    zs "github.com/DataDog/zstd"
    "io"
)

func Compress(in io.Reader, out io.Writer, done func()){
    defer done()
    wr:=zs.NewWriter(out)
    var buf [128]byte
    io.CopyBuffer(wr, in, buf[:])
    wr.Close()
}
