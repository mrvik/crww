package crww

import (
    "gitlab.com/mrvik/crww/br"
    "gitlab.com/mrvik/crww/gz"
    "gitlab.com/mrvik/crww/zstd"
    "io"
    "strings"
)

var (
    //Initial plan for methods and their functions
    //As this var is public, you may want to add (or remove) custom encoders
    Methods=[]CompressorSpec{
        {
            Name: "br",
            Fn: br.Compress,
        },
        {
            Name: "zstd",
            Fn: zstd.Compress,
        },
        {
            Name: "gzip",
            Fn: gz.Compress,
        },
    }
)

//Spec for compression method
type CompressorSpec struct{
    //Name must match the name for the given method on Content-Encoding and Accept-Encoding options
    Name string
    //Function to use the compressor
    Fn CompressorFunc
}

//CompressorFunc reads from inReader and writes encoded data to writeTo writer
//This func will be called in a separate goroutine
//Function should panic if internal error occurred. Must call done function when finished writing output
type CompressorFunc func(inReader io.Reader, writeTo io.Writer, done func())

//Detect accepted formats and use the best compression method supported
//If encoding=="" then compressor==nil and no compression is supported/matched
func DetectCompression(accept string)(encoding string, compressor CompressorFunc){
    if accept==""{
        return //Avoid iteration over Methods
    }
    for _,method:=range(Methods){
        if strings.Contains(accept, method.Name) {
            encoding, compressor=method.Name, method.Fn
            break
        }
    }
    return
}
