// Package adapter contains structures to receive http requests (from net/http package) and wrap their ResponseWriter
// before calling the next-hop.
package adapter

import (
    "gitlab.com/mrvik/crww"
    "net/http"
)

//FnWrapper takes a Fn argument and handles requests by wrapping the response writter before calling Fn with the CRWW.
//FnWrapper calls CRWW.Close method
type FnWrapper struct{
    Fn func(rw http.ResponseWriter, req *http.Request)
}

//ServeHTTP implements http.Handler. Calls CRWW when finished.
//On error panics
func (fnw FnWrapper) ServeHTTP(rw http.ResponseWriter, req *http.Request){
    wrapper:=crww.NewCRWW(rw, req)
    fnw.Fn(wrapper, req)
    err:=wrapper.Close()
    if err!=nil{
        panic(err)
    }
}

//HandlerWrapper is the same as FnWrapper but for implementations of http.Handler
type HandlerWrapper struct{
    Handler http.Handler
}

//ServeHTTP implements http.Handler and calls CRWW when finished
func (hw HandlerWrapper) ServeHTTP(rw http.ResponseWriter, req *http.Request){
    wrapper:=crww.NewCRWW(rw, req)
    hw.Handler.ServeHTTP(wrapper, req)
    err:=wrapper.Close()
    if err!=nil{
        panic(err)
    }
}
