package br

import (
    "github.com/itchio/go-brotli/enc"
    "io"
)

func Compress(in io.Reader, out io.Writer, done func()){
    defer done()
    var (
        encoder=enc.NewBrotliWriter(out, &enc.BrotliWriterOptions{
            Quality: 6,
        })
        buf [512]byte
    )
    _, err:=io.CopyBuffer(encoder, in, buf[:])
    if err!=nil{
        panic(err)
    }
    encoder.Close()
}
